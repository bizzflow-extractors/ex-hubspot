import logging
import os
import sys

import dateparser
from bizztreat_base.config import Config
from hubspot import HubSpot

from src.base_extractor import Extractor
from src.special_extractors import (ContactsExtractor, LineItemsExtractor,
                                    OwnersExtractor, PipelineDealsExtractor)

proxy = Config(force_schema=True)

if proxy.config.get("debug"):
    log_level = logging.DEBUG
else:
    log_level = logging.INFO
logging.basicConfig(level=log_level, stream=sys.stdout)
logger = logging.getLogger(__name__)


def main():
    if not os.path.exists(proxy.args.output):
        os.makedirs(proxy.args.output)
    start_time = get_start_time()
    api_client = HubSpot(access_token=proxy.config["access_token"])
    special_extractors = {
        "line_items": LineItemsExtractor,
        "pipelines_deals": PipelineDealsExtractor,
        "owners": OwnersExtractor,
        "archived_owners": OwnersExtractor,
        "contacts": ContactsExtractor,
    }

    for key in proxy.config["objects"]:
        extractor_instance = special_extractors.get(key, Extractor)(
            key, proxy, start_time, api_client
        )
        extractor_instance.extract()


def get_start_time() -> int:
    try:
        start_time = proxy.config["start_time"]
    except KeyError:
        return 0
    else:
        return int(
            dateparser.parse(
                start_time, settings={"RETURN_AS_TIMEZONE_AWARE": True}
            ).timestamp()
            * 1000
        )


if __name__ == "__main__":
    main()
