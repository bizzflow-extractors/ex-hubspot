import csv
import logging
import os

logger = logging.getLogger(__name__)


class dict_writer:
    def __init__(self, file_basename: str, proxy):
        self.file = None
        self.columns = None
        self.inner_writer = None
        self.proxy = proxy
        self.filepath = self._get_filepath(file_basename)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.file:
            self.file.close()
            logger.info(f"Closed file {self.filepath}")

    def writerow(self, row: dict):
        if self.inner_writer:
            self.inner_writer.writerow(row)
        else:
            logger.info(f"Opening file {self.filepath}")
            self.file = open(self.filepath, "w")
            self.inner_writer = csv.DictWriter(
                self.file, dialect=csv.unix_dialect, fieldnames=row.keys()
            )
            self.inner_writer.writeheader()
            self.inner_writer.writerow(row)
            self.columns = set(row.keys())
        self._check_keys(row)

    def _check_keys(self, row: dict):
        if set(row.keys()) != self.columns:
            raise KeyError(
                f"Last dictionary written to {self.filepath} was missing keys {self.columns - row.keys()}"
            )

    def _get_filepath(self, name):
        return os.path.join(self.proxy.output_folder, f"{name}.csv")
