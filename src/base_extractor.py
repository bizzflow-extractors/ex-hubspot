import json
import logging
import time
from contextlib import ExitStack
from typing import Iterable, Iterator

from bizztreat_base.utils.writer import lazy_dict_writer

from src.dict_writer import dict_writer

logger = logging.getLogger(__name__)


class Extractor:
    RESPONSE_SIZE = 100  # hubspot maximum
    SEARCH_API_LIMIT = 10000

    def __init__(self, keyword: str, proxy, start_time_miliseconds: int, api_client):
        self.api_client = api_client
        self.base_endpoint = getattr(
            self.api_client.crm,
            keyword,
            getattr(self.api_client.crm.objects, keyword, None),
        )
        if not self.base_endpoint:
            logger.info(
                f"Endpoint for {keyword} not found, specify download_data method in a child class"
            )
        self.name = keyword
        self.csv_name = f"{keyword}.csv"
        self.last_modified_date_name = "hs_lastmodifieddate"
        self.start_time_miliseconds = start_time_miliseconds
        self.proxy = proxy
        key_config = proxy.config["objects"][keyword]
        self.associations = key_config.get("associations", [])
        self.properties_to_add = key_config.get("add_properties", [])
        self.column_order_dict = dict.fromkeys(key_config.get("column_order", []))
        self.properties_with_history = key_config.get("properties_with_history", [])
        self.archived = key_config.get("archived", False)
        self.properties_with_history_writers = {}

    def extract(self):
        data = self.download_data()
        with ExitStack() as stack:
            for property_name in self.properties_with_history:
                writer = lazy_dict_writer(
                    self.proxy.args.output, f"{self.name}_{property_name}_history"
                )
                self.properties_with_history_writers[property_name] = writer
                stack.enter_context(writer)
            data = self.format_data(data)
            self.write_csv(data)

    def download_data(self) -> Iterator[dict]:
        if self.properties_with_history:
            yield from self.download_all()
        else:
            yield from self.download_increment()

    def download_increment(self):
        for i in range(0, self.SEARCH_API_LIMIT, self.RESPONSE_SIZE):
            timeout_start_time = time.monotonic()
            logger.info(f"Requesting {self.name} {i}-{i + self.RESPONSE_SIZE}")
            page = self.base_endpoint.search_api.do_search(
                {
                    "properties": self.properties_to_add,
                    "filters": [
                        {
                            # not sure about the difference to "updated_at", but this was in support response
                            # https://community.hubspot.com/t5/APIs-Integrations/Pulling-incremental-data-of-company-and-contacts/m-p/591750
                            "propertyName": self.last_modified_date_name,
                            "operator": "GT",  # = greater
                            "value": self.start_time_miliseconds,
                        }
                    ],
                    "limit": self.RESPONSE_SIZE,
                    "after": i,  # pagination
                }
            )
            if i == 0:  # first iteration
                promised_items = page.total
                logger.info(f"Search api promises {promised_items} {self.name}")
                if promised_items == 0:
                    return
                if promised_items > self.SEARCH_API_LIMIT:
                    # logger.info(f"Search api limit: {self.search_api_limit}. Downloading all {self.name}")
                    # results = self.base_endpoint.get_all(archived=self.archived,
                    #                                     properties=self.properties_to_add,
                    #                                     associations=self.associations)
                    # return results
                    raise IndexError(
                        f"To many results. Please make start_time in config more recent"
                    )
            results = self.format_page(page)["results"]
            logger.info(f"Processing {self.name} {i}-{i + len(results)} ")
            if self.associations:
                results = self.append_associations(results)
            for result in results:
                yield result
            if (i + self.RESPONSE_SIZE) > promised_items:
                return
            if promised_items > self.SEARCH_API_LIMIT:
                return
            end_time = time.monotonic()
            duration = end_time - timeout_start_time
            if duration < 0.26:
                logger.debug("Waiting for hubspot: 4 requests per second limit")
                time.sleep(0.26 - duration)
        else:
            raise IndexError(
                "Paging is over limit. Please report to tech-team@bizztreat.com"
            )

    def download_all(self) -> Iterator[dict]:
        logger.info(f"Downloading all {self.name} with properties with history")
        after = None
        while True:
            logger.info(f"Downloading after {after}")
            page = self.api_client.crm.deals.basic_api.get_page(
                properties=self.properties_to_add,
                associations=self.associations,
                properties_with_history=self.properties_with_history,
                limit=50,
                after=after,
            )
            for result in page.results:
                yield result.to_dict()
            if not page.paging:
                return
            after = page.paging.next.after

    @staticmethod
    def format_page(page) -> dict:
        return page.to_dict()

    def append_associations(self, page_results: list[dict]) -> list[dict]:
        result_ids = [result["properties"]["hs_object_id"] for result in page_results]
        for association_type in self.associations:
            associations_reply = self.download_association(result_ids, association_type)
            for i, result in enumerate(page_results):
                hs_object_id = result["properties"]["hs_object_id"]
                for association in associations_reply.results:
                    if association._from.id == hs_object_id:
                        # assuming that all associations are listed in association.to (attribute 'to' is list)
                        association_ids = [item.id for item in association.to]
                        page_results[i] = result | self.format_associations(
                            hs_object_id, association_type, association_ids
                        )
                        break
                else:
                    page_results[i] = result | self.format_associations(
                        hs_object_id, association_type, []
                    )
        return page_results

    def format_associations(
        self, hs_object_id: str, association_type: str, association_ids: list[str]
    ) -> dict:
        return {
            f"asso_{association_type}_id": ",".join(association_ids),
            f"{self.name}_{association_type}_id": "_".join(
                [hs_object_id] + association_ids
            ),
        }

    def download_association(
        self, result_ids: list[int], association_type
    ) -> list[object]:
        associations = self.api_client.crm.associations.batch_api.read(
            self.name,
            association_type,
            batch_input_public_object_id={"inputs": result_ids},
        )
        logger.debug(associations)
        return associations

    def format_data(self, data: Iterable[dict]) -> Iterator[dict]:
        for row in data:
            row = self.format_values(row)
            yield self.column_order_dict | row

    @staticmethod
    def recursive_format(row: dict) -> dict:
        for key, value in row.items():
            value_type = type(value)
            if value_type == list:
                row[key] = json.dumps(value, default=str)
            if value_type == dict:
                intersect = set(row.keys()).intersection(set(value.keys()))
                if intersect:
                    raise KeyError(
                        f"Key {intersect} was overwritten. Data lost. Report to tech"
                    )
                row = row | value
                del row[key]
        return row

    def format_values(self, row: dict) -> dict:
        if self.properties_with_history:
            properties_with_history = row.pop("properties_with_history", {})
            self.write_properties_with_history(properties_with_history, row["id"])
        return self.recursive_format(row)

    def write_csv(self, data: Iterable[dict]):
        with dict_writer(self.name, self.proxy) as default_writer:
            for row in data:
                default_writer.writerow(row)

    @staticmethod
    def make_camelCase(row: dict) -> dict:
        camelCase_row = {}
        for key in row.keys():
            words = key.split("_")
            first_word = words.pop(0)
            final_words = [word[0].upper() + word[1:] for word in words]
            camelCase = first_word + "".join(final_words)
            camelCase_row[camelCase] = row[key]
        return camelCase_row

    def write_properties_with_history(
        self, properties_with_history: dict, parent_id: int
    ):
        parent_id_dict = {"parent_id": parent_id}
        for property_name, property_history in properties_with_history.items():
            for entry_dict in property_history:
                entry_dict = parent_id_dict | entry_dict.to_dict()
                self.properties_with_history_writers[property_name].writerow(entry_dict)
