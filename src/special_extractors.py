import logging
from typing import Iterable, Iterator

from src.base_extractor import Extractor
from src.dict_writer import dict_writer

logger = logging.getLogger(__name__)


class LineItemsExtractor(Extractor):
    def __init__(self, key: str, proxy, start_time_miliseconds: int, api_client):
        super().__init__(key, proxy, start_time_miliseconds, api_client)

    def extract(self):
        data = self.download_data()
        self.format_and_write(data)

    def format_and_write(self, data: Iterable[dict]):
        with dict_writer(self.name, self.proxy) as line_items_writer:
            with dict_writer(
                "gd_workspaces_deals", self.proxy
            ) as gooddata_workspaces_writer:
                for row in data:
                    row = self.column_order_dict | self.format_values(row)
                    line_items_writer.writerow(row)

                    if row.get("gooddata_workspaces"):
                        gooddata_workspaces = row["gooddata_workspaces"].splitlines()
                        for workspace in gooddata_workspaces:
                            row["gooddata_workspaces"] = workspace
                            gooddata_workspaces_writer.writerow(row)


class PipelineDealsExtractor(Extractor):
    def __init__(self, key: str, proxy, start_time_miliseconds: int, api_client):
        super().__init__(key, proxy, start_time_miliseconds, api_client)
        self.pipeline_deals_column_order = dict.fromkeys(
            [
                "archived",
                "createdAt",
                "displayOrder",
                "id",
                "internalWriteOnly",
                "label",
                "metadata_isClosed",
                "metadata_probability",
                "pipeline_id",
                "updatedAt",
            ]
        )

    def extract(self):
        data = self.download_data()
        self.format_and_write(data)

    def download_data(self) -> Iterator[dict]:
        page = self.api_client.crm.pipelines.pipelines_api.get_all("deals").results
        logger.info(f"Processing {len(page)} {self.name}")
        return (row.to_dict() for row in page)

    def format_and_write(self, data: Iterable[dict]):
        with dict_writer("pipelines_deals_stages", self.proxy) as stage_writer:
            with dict_writer(self.name, self.proxy) as pipelines_deals_writer:
                for row in data:
                    stages = (
                        self.pipeline_deals_column_order
                        | {"pipeline_id": row.get("id")}
                        | {k: v for k, v in stage.items() if k != "metadata"}
                        | {
                            f"metadata_{k}": v
                            for k, v in stage.get("metadata", {}).items()
                        }
                        for stage in row.get("stages")
                    )
                    for stage in stages:
                        stage_writer.writerow(stage)

                    row = self.column_order_dict | self.format_values(row)
                    pipelines_deals_writer.writerow(row)

    def format_values(self, row: dict) -> dict:
        row = super().format_values(row)
        return self.make_camelCase(row)


class OwnersExtractor(Extractor):
    def __init__(self, key: str, proxy, start_time_miliseconds: int, api_client):
        super().__init__(key, proxy, start_time_miliseconds, api_client)

    def download_data(self) -> Iterator[dict]:
        if self.name == "archived_owners":
            self.archived = True
        logger.info(f"Downloading all {self.name}")
        page = self.api_client.crm.owners.get_all(archived=self.archived)
        return (row.to_dict() for row in page)

    def format_values(self, row: dict) -> dict:
        super().format_values(row)
        return self.make_camelCase(row)


class ContactsExtractor(Extractor):
    def __init__(self, key: str, proxy, start_time_miliseconds: int, api_client):
        super().__init__(key, proxy, start_time_miliseconds, api_client)
        self.last_modified_date_name = "lastmodifieddate"
