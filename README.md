# HubSpot Extractor

## Description

Extractor downloads objects from various endpoints,
their properties and associations via hupspot-api-client into .csv files.
as specified in config.json:

* owners
* companies
* deals
* line items
* pipeline deals
* products

## Requirements

HubSpot Access Token with properly set scopes. See [HubSpot API Scopes](https://developers.hubspot.com/docs/api/private-apps)

## How to use it

Create or modify `config.json` file. 
Use your API token as api_key value.
Specify start_time to set how old the downloaded objects may be.
Specify objects to be downloaded in the config file.
For more info visit [HubSpot API docu](https://developers.hubspot.com/docs/api/overview),
in particular `Owners`, `Objects`, `Properties`, and `Associations` endpoints.

### Config

For config sample please see config-sample.json .

If you need .cvs columns in particular order, please use property "column_order".

If you set "properties_with_history" to non-empty array, extractor will turn off incremental processing for
affected object and will download all available data for object.
This is because HubSpot API does not support incremental processing for properties with history.

#### Extra - GoodData workspaces' ids associated with deal instance
If `gooddata_workspaces` property is specified for `line_items`,
GoodData workspace ids for given deal instance are downloaded as well.
Ids must be stored in  custom multiline field called `gooddata_workspaces`
and separated by new line (one id per line).
