FROM python:3.10-slim

LABEL com.bizztreat.type="Extractor"
LABEL com.bizztreat.purpose="Bizzflow"
LABEL com.bizztreat.component="ex-hubspot"
LABEL com.bizztreat.title="Hubspot Extractor"

RUN apt-get update && apt-get -y install curl && curl -sSL https://install.python-poetry.org | python -

ENV POETRY_VIRTUALENVS_CREATE=false

WORKDIR /code

ADD . .

RUN /root/.local/bin/poetry install --no-root

# Edit this any way you like it
ENTRYPOINT ["python", "-um", "src.main"]
